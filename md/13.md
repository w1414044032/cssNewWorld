---
title: 全局关键词
---
#### 全局关键词

inherit、initial、unset和revert都是CSS全局关键字属性值，也就是说所有CSS属性都可以使用这几个关键字作为属性值。
1. inherit: 继承

2. initial: 初始值[还原成CSS语法中规定的初始值。]
CSS 关键字 initial 将属性的初始（或默认）值应用于元素。不应将初始值与浏览器样式表指定的值混淆。


