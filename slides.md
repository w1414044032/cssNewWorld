---
# try also 'default' to start simple
theme: default
# random image from a curated Unsplash collection by Anthony
# like them? see https://unsplash.com/collections/94734566/slidev
background: https://img11.360buyimg.com/n1/jfs/t1/48257/21/13663/143464/5da5421bE460298a8/188b8e81859a3051.jpg
# apply any windi css classes to the current slide
class: 'text-center'
# https://sli.dev/custom/highlighters.html
highlighter: shiki
# show line numbers in code blocks
lineNumbers: false
# some information about the slides, markdown enabled
info: |
  ## Slidev Starter Template
  Presentation slides for developers.

  Learn more at [Sli.dev](https://sli.dev)
# persist drawings in exports and build
drawings:
  persist: false
# use UnoCSS (experimental)
css: unocss

monaco: true
---
# CSS新世界
[https://weread.qq.com/web/bookDetail/13c32c90726fa07d13c0072](https://weread.qq.com/web/bookDetail/13c32c90726fa07d13c0072)

---
src: ./md/1.md
---

---
src: ./md/2.md
---

---
src: ./md/3.md
---

---
src: ./md/4.md
---

---
src: ./md/5.md
---

---
src: ./md/6.md
---

---
src: ./md/7.md
---

---
src: ./md/8.md
---

---
src: ./md/9.md
---

---
src: ./md/10.md
---

---
src: ./md/11.md
---

---
src: ./md/12.md
---

---
src: ./md/13.md
---

---
src: ./md/14.md
---

---
src: ./md/15.md
---

---
src: ./md/16.md
---

---
src: ./md/17.md
---

---
src: ./md/18.md
---

---
src: ./md/19.md
---