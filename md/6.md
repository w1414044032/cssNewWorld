---
title: 关键词
---


# 语法包含元素

1. 关键词
2. 数据类型
3. 符号

-----
#### 普通关键词

auto: 浏览器自动计算,“自动”占用可用空间
<div class="grid grid-cols-2 grid-rows-1 gap-5">
 <div>
<div class="self-auto bg-green">
    父容器
    <div class="content bg-purple-500">
        子容器aa
    </div>
</div>
</div>
<div>
<div class="self-auto bg-green">
    父容器
    <div class="hundred-content bg-purple-500">
        子容器a
    </div>
</div>
</div>
</div>

<div class="grid gird-cols-2">
<div>
<div v-click>
1. auto自动充满父容器。包含content margin padding.

</div>
<div v-click>
2. 100%宽度的子容器,content对齐父容器的content长度，会溢出父容器.

</div>
<div v-click>
3. 浮动情况下, 默认包裹内容。100%保持对齐父容器宽度。
</div>

</div>
<div>

</div>
</div>
 

<style>
.self-auto{
    width: 200px;
    border: deepskyblue 10px solid;
    padding: 1px;
}
.hundred-content{
    width: 100%;
  margin:10px;
}
  .content{
    margin:10px;
  }
  .box-border{
    width: 50px;
    height:50px;
  }
  .box-content{
    width: 50px;
    height:50px;
  }
  .bg-yellow{
    padding: 4px;
    border: 4px solid red;
  }
  
  
  

</style>

